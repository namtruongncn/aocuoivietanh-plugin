<?php
/**
 * PD CORE class.
 *
 * @package PD_CORE
 */

/**
 * Class PD_CORE
 */
final class PD_CORE {

	/**
	 * Plugin instance.
	 *
	 * @var PD_CORE
	 * @access private
	 */

	private static $instance = null;
	/**
	 * Get plugin instance.
	 *
	 * @return PD_CORE
	 * @static
	 */

	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	/**
	 * Constructor.
	 *
	 * @access private
	 */
	private function __construct() {
		add_action( 'plugins_loaded', array( $this, 'init' ) );
		add_action( 'init', array( $this, 'pdcore_load_plugin_textdomain' ) );
	}

	/**
	 * Code you want to run when all other plugins loaded.
	 */
	public function init() {
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

		require_once PD_CORE_PATH . 'vendor/load.php';
		require_once PD_CORE_PATH . 'includes/abstract/abstract-pd-widget.php';
		require_once PD_CORE_PATH . 'includes/class-pd-autoloader.php';
		require_once PD_CORE_PATH . 'includes/pd-widget-functions.php';
		require_once PD_CORE_PATH . 'includes/shortcode.php';
		require_once PD_CORE_PATH . 'includes/posttype.php';
		require_once PD_CORE_PATH . 'includes/metabox.php';

		do_action( 'pdcore_init' );
	}

	/**
	 * Load Plugin Textdomain.
	 */
	function pdcore_load_plugin_textdomain() {
		load_plugin_textdomain( 'phoenixdigi', false, PD_CORE_PATH . 'languages' );
	}

	/**
	 * Enqueue all main and scripts.
	 */
	public function enqueue_scripts() {
		wp_enqueue_style( 'slick', PD_CORE_URL . 'assets/css/slick.min.css', array(), '1.6.0' );
		wp_enqueue_style( 'pdcore-main', PD_CORE_URL . 'assets/css/main.css', array(), '1.0.0' );
		wp_enqueue_script( 'slick',  PD_CORE_URL . 'assets/js/slick.min.js', array( 'jquery' ), '1.6.0', true );
		wp_enqueue_script( 'pdcore-main', PD_CORE_URL . 'assets/js/main.js', array( 'jquery' ), '1.0.0', true );
	}
}
