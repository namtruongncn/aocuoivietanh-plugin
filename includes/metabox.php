<?php
/**
 * Register metabox.
 */

/**
 * [pdcore_register_album_metabox description]
 * @param  PDFW   $pdfw [description]
 * @return [type]       [description]
 */
function pdcore_register_album_metabox( PDFW $pdfw ) {
	$screen = apply_filters( 'pd_album_metabox_screen', array( 'album' ) );

	$args = array(
		'title'   => esc_html__( 'Thông tin thêm', 'phoenixdigi' ),
		'screen'  => $screen,
		'fields'  => array(
			array(
				'id'      => 'link',
				'type'    => 'text',
				'title'   => esc_html__( 'Link Video', 'phoenixdigi' ),
				'default' => 'https://www.youtube.com/watch?v=l7716ZbKYh8',
			),
		),
		'context' => 'advanced',
	);

	$pdfw->register_metabox( new PDFW_Metabox( 'album_info', $args ) );
}
add_action( 'pdfw_init', 'pdcore_register_album_metabox' );

/**
 * [pdcore_register_page_metabox description]
 * @param  PDFW   $pdfw [description]
 * @return [type]       [description]
 */
function pdcore_register_page_metabox( PDFW $pdfw ) {
	$screen = apply_filters( 'pd_page_metabox_screen', array( 'page' ) );

	$args = array(
		'title'   => esc_html__( 'Khu vực đầu trang tĩnh', 'phoenixdigi' ),
		'screen'  => $screen,
		'fields'  => array(
			array(
				'id'      => 'image',
				'type'    => 'image',
				'title'   => esc_html__( 'Hình ảnh lớn đầu trang', 'phoenixdigi' ),
			),
		),
		'context' => 'advanced',
	);

	$pdfw->register_metabox( new PDFW_Metabox( 'jumbotron', $args ) );
}
add_action( 'pdfw_init', 'pdcore_register_page_metabox' );
