<?php
/**
 * PD-Theme Widget Functions
 *
 * Widget related functions and widget registration.
 *
* @author 	NamNCN
 * @category 	Core
 * @version 	1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Register Widgets.
 *
 * @since 1.0.0
 */
function pdcore_register_widgets() {
	register_widget( 'PD_Fbpage_Widget' );
	register_widget( 'PD_Facebook_Comment_Widget' );
	register_widget( 'PD_Recent_Posts_Widget' );
	register_widget( 'PD_Post_By_Category_Widget' );
	register_widget( 'PD_Post_By_Category_2_Widget' );
	register_widget( 'PD_Popular_Posts_Widget' );
	register_widget( 'PD_Related_Posts_Widget' );
	register_widget( 'PD_Posts_Slider_Widget' );

	if ( function_exists( 'pdfw' ) ) {
		register_widget( 'PD_Support_Widget' );
		register_widget( 'PD_Textarea_Widget' );
		register_widget( 'PD_Video_Widget' );
		register_widget( 'PD_Partner_Widget' );
	}

	if ( class_exists( 'MetaSliderPlugin' ) ) {
		unregister_widget( 'MetaSlider_Widget' );
		register_widget( 'PD_Slider_Widget' );
	}

	if ( function_exists( 'WC' ) ) {
		register_widget( 'PD_Best_Seller_Products_Widget' );
		register_widget( 'PD_Recent_Products_Widget' );
		register_widget( 'PD_Products_By_Category_Widget' );
	}

}
add_action( 'widgets_init', 'pdcore_register_widgets' );
