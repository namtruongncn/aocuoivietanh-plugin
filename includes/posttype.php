<?php
/**
 * Register post type.
 */

/**
* Registers a new post type
* @uses $wp_post_types Inserts new post type object into the list
*
* @param string  Post type key, must not exceed 20 characters
* @param array|string  See optional args description above.
* @return object|WP_Error the registered post type object, or an error object
*/
function pd_register_service() {

	$labels = array(
		'name'                => __( 'Dịch vụ', 'pd-theme' ),
		'singular_name'       => __( 'dịch vụ', 'pd-theme' ),
		'add_new'             => _x( 'Thêm mới dịch vụ', 'pd-theme', 'pd-theme' ),
		'add_new_item'        => __( 'Thêm mới dịch vụ', 'pd-theme' ),
		'edit_item'           => __( 'Chỉnh sửa dịch vụ', 'pd-theme' ),
		'new_item'            => __( 'Thêm mới dịch vụ', 'pd-theme' ),
		'view_item'           => __( 'Xem dịch vụ', 'pd-theme' ),
		'search_items'        => __( 'Tìm kiếm dịch vụ', 'pd-theme' ),
		'not_found'           => __( 'Không tồn tại dịch vụ', 'pd-theme' ),
		'not_found_in_trash'  => __( 'Không tìm thấy dịch vụ trong bộ nhớ tạm', 'pd-theme' ),
		'parent_item_colon'   => __( 'Dịch vụ mẹ:', 'pd-theme' ),
		'menu_name'           => __( 'Dịch vụ', 'pd-theme' ),
	);

	$args = array(
		'labels'                   => $labels,
		'hierarchical'        => false,
		'description'         => 'description',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 6,
		'menu_icon'           => 'dashicons-list-view',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post',
		'supports'            => array(
			'title', 'editor', 'thumbnail'
			)
	);

	register_post_type( 'dich-vu', $args );
}
add_action( 'init', 'pd_register_service' );

/**
* Registers a new post type
* @uses $wp_post_types Inserts new post type object into the list
*
* @param string  Post type key, must not exceed 20 characters
* @param array|string  See optional args description above.
* @return object|WP_Error the registered post type object, or an error object
*/
function pd_register_albums() {

	$labels = array(
		'name'                => __( 'Albums', 'pd-theme' ),
		'singular_name'       => __( 'album', 'pd-theme' ),
		'add_new'             => _x( 'Thêm mới album', 'pd-theme', 'pd-theme' ),
		'add_new_item'        => __( 'Thêm mới album', 'pd-theme' ),
		'edit_item'           => __( 'Chỉnh sửa album', 'pd-theme' ),
		'new_item'            => __( 'Thêm mới album', 'pd-theme' ),
		'view_item'           => __( 'Xem album', 'pd-theme' ),
		'search_items'        => __( 'Tìm kiếm album', 'pd-theme' ),
		'not_found'           => __( 'Không tồn tại album', 'pd-theme' ),
		'not_found_in_trash'  => __( 'Không tìm thấy album trong bộ nhớ tạm', 'pd-theme' ),
		'parent_item_colon'   => __( 'Parent album:', 'pd-theme' ),
		'menu_name'           => __( 'Album', 'pd-theme' ),
	);

	$args = array(
		'labels'                   => $labels,
		'hierarchical'        => false,
		'description'         => 'description',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 7,
		'menu_icon'           => 'dashicons-format-gallery',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post',
		'supports'            => array(
			'title', 'editor', 'thumbnail'
			)
	);

	register_post_type( 'album', $args );
}
add_action( 'init', 'pd_register_albums' );

/**
* Registers a new post type
* @uses $wp_post_types Inserts new post type object into the list
*
* @param string  Post type key, must not exceed 20 characters
* @param array|string  See optional args description above.
* @return object|WP_Error the registered post type object, or an error object
*/
function pd_register_testimonials() {

	$labels = array(
		'name'                => __( 'Ý kiến khách hàng', 'pd-theme' ),
		'singular_name'       => __( 'ý kiến khách hàng', 'pd-theme' ),
		'add_new'             => _x( 'Thêm mới ý kiến khách hàng', 'pd-theme', 'pd-theme' ),
		'add_new_item'        => __( 'Thêm mới ý kiến khách hàng', 'pd-theme' ),
		'edit_item'           => __( 'Chỉnh sửa ý kiến khách hàng', 'pd-theme' ),
		'new_item'            => __( 'Thêm mới ý kiến khách hàng', 'pd-theme' ),
		'view_item'           => __( 'Xem ý kiến khách hàng', 'pd-theme' ),
		'search_items'        => __( 'Tìm kiếm ý kiến khách hàng', 'pd-theme' ),
		'not_found'           => __( 'Không tồn tại ý kiến khách hàng', 'pd-theme' ),
		'not_found_in_trash'  => __( 'Không tìm thấy ý kiến khách hàng trong bộ nhớ tạm', 'pd-theme' ),
		'parent_item_colon'   => __( 'Parent ý kiến khách hàng:', 'pd-theme' ),
		'menu_name'           => __( 'Ý kiến khách hàng', 'pd-theme' ),
	);

	$args = array(
		'labels'                   => $labels,
		'hierarchical'        => false,
		'description'         => 'description',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 8,
		'menu_icon'           => 'dashicons-format-status',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post',
		'supports'            => array(
			'title', 'editor', 'thumbnail'
			)
	);

	register_post_type( 'testimonial', $args );
}
add_action( 'init', 'pd_register_testimonials' );

/**
* Registers a new post type
* @uses $wp_post_types Inserts new post type object into the list
*
* @param string  Post type key, must not exceed 20 characters
* @param array|string  See optional args description above.
* @return object|WP_Error the registered post type object, or an error object
*/
function pd_register_price_tag() {

	$labels = array(
		'name'                => __( 'Bảng giá', 'pd-theme' ),
		'singular_name'       => __( 'Bảng giá', 'pd-theme' ),
		'add_new'             => _x( 'Thêm mới Bảng giá', 'pd-theme', 'pd-theme' ),
		'add_new_item'        => __( 'Thêm mới Bảng giá', 'pd-theme' ),
		'edit_item'           => __( 'Chỉnh sửa Bảng giá', 'pd-theme' ),
		'new_item'            => __( 'Thêm mới Bảng giá', 'pd-theme' ),
		'view_item'           => __( 'Xem Bảng giá', 'pd-theme' ),
		'search_items'        => __( 'Tìm kiếm Bảng giá', 'pd-theme' ),
		'not_found'           => __( 'Không tồn tại Bảng giá', 'pd-theme' ),
		'not_found_in_trash'  => __( 'Không tìm thấy Bảng giá trong bộ nhớ tạm', 'pd-theme' ),
		'parent_item_colon'   => __( 'Parent Bảng giá:', 'pd-theme' ),
		'menu_name'           => __( 'Bảng giá', 'pd-theme' ),
	);

	$args = array(
		'labels'                   => $labels,
		'hierarchical'        => false,
		'description'         => 'description',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 10,
		'menu_icon'           => 'dashicons-calendar',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post',
		'supports'            => array(
			'title', 'editor', 'thumbnail'
			)
	);

	register_post_type( 'bang-gia', $args );
}
add_action( 'init', 'pd_register_price_tag' );
