<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * PD Facebook Comment Widget.
 *
 * Showing facebook comment.
 *
 * @link https://github.com/namncn/ncn-facebook-comment
 *
 * @author   NamNCN
 * @category Widgets
 * @package  PDCORE/Widgets
 * @version  1.0.0
 * @extends  NCN_Widget
 */
class PD_Facebook_Comment_Widget extends PD_Widget {

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->widget_cssclass    = 'pd_facebook_comment';
		$this->widget_description = esc_html__( "Hiển thị bình luận Facebook.", 'phoenixdigi' );
		$this->widget_id          = 'pd_facebook_comment';
		$this->widget_name        = esc_html__( 'PD: Bình luận Facebook', 'phoenixdigi' );
		$this->settings           = array(
			'title'  => array(
				'type'  => 'text',
				'std'   => esc_html__( 'Bình luận', 'phoenixdigi' ),
				'label' => esc_html__( 'Tiêu đề', 'phoenixdigi' ),
			),
			'data_numposts' => array(
				'type'          => 'number',
				'min'           => 1,
				'max'           => 1000,
				'step'          => 1,
				'std'           => 5,
				'label'         => esc_html__( 'Số bình luận muốn hiển thị', 'phoenixdigi' ),
			),
			'data_width' => array(
				'type'   => 'text',
				'std'    => '100%',
				'desc'   => esc_html__( 'Chiều rộng của khung bình luận. Tối thiểu là 320. Bỏ qua nếu bạn muốn cho chiều rộng rộng 100% bằng với khung chứa.', 'phoenixdigi' ),
				'label'  => esc_html__( 'Chiều rộng:', 'phoenixdigi' ),
			),
			'data_order_by'  => array(
				'type'   => 'select',
				'std'    => 'social',
				'label'  => esc_html__( 'Sắp xếp bình luận theo thứ tự', 'phoenixdigi' ),
				'options' => array(
					'social'       => esc_html__( 'Hàng đầu', 'phoenixdigi' ),
					'reverse_time' => esc_html__( 'Mới nhất', 'phoenixdigi' ),
					'time'         => esc_html__( 'Cũ nhất', 'phoenixdigi' ),
				),
			),
			'data_colorscheme'  => array(
				'type'  => 'select',
				'std'   => 'light',
				'label' => esc_html__( 'Chọn màu sắc khung bình luận:', 'phoenixdigi' ),
				'options' => array(
					'light' => esc_html__( 'Trắng', 'phoenixdigi' ),
					'dark'  => esc_html__( 'Đen', 'phoenixdigi' ),
				),
			),
		);

		parent::__construct();
	}

	/**
	 * Output widget.
	 *
	 * @see WP_Widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {

		// extract( $instance ); Don't extract variable $args, $instance cuz its not work when selective refresh.
		$defaults = array(
			'data_numposts'    => 5,
			'data_width'       => '100%',
			'data_order_by'    => 'social',
			'data_colorscheme' => 'light',
		);

		$instance = wp_parse_args( $instance, $defaults );

		$this->widget_start( $args, $instance );

		if ( $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		?>

		<div class="fb-comments" data-href="<?php the_permalink(); ?>" data-order-by="<?php echo esc_attr( $instance['data_order_by'] ); ?>" data-colorscheme="<?php echo esc_attr( $instance['data_colorscheme'] ); ?>" data-width="<?php echo esc_attr( $instance['data_width'] ); ?>" data-numposts="<?php echo esc_attr( $instance['data_numposts'] ); ?>"></div>

		<?php
		$this->widget_end( $args );
	}
}
