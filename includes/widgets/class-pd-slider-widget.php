<?php
/**
 * Widget class.
 *
 * @package Phoenix_Digi
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * PD Slider Posts Widget.
 *
 * Show slider posts.
 *
 * @author   NamNCN
 * @category Widgets
 * @package  PDCORE/Widgets
 * @version  1.0.0
 * @extends  PD_Widget
 */
class PD_Slider_Widget extends PD_Widget {
	/**
	 * Sliders Object.
	 */
	public function sliders() {
		return get_posts( array(
			'post_type'      => 'ml-slider',
			'post_status'    => 'publish',
			'orderby'        => 'date',
			'order'          => 'ASC',
			'posts_per_page' => -1,
		) );
	}

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->widget_cssclass    = 'pd_slider_widget';
		$this->widget_description = esc_html__( "Hiển thị slider.", 'phoenixdigi' );
		$this->widget_id          = 'pd_slider_widget';
		$this->widget_name        = esc_html__( 'PD: Slider', 'phoenixdigi' );

		$this->settings           = array(
			'title'  => array(
				'type'  => 'text',
				'std'   => esc_html__( 'Slider', 'phoenixdigi' ),
				'label' => esc_html__( 'Tiêu đề:', 'phoenixdigi' ),
			),
			'id' => array(
				'type'   => 'select',
				'std'    => $this->slider_id_default(),
				'options' => $this->slider_ids(),
				'label'  => esc_html__( 'Chọn Slider muốn hiển thị:', 'phoenixdigi' ),
			),
		);

		parent::__construct();
	}

	/**
	 * Output widget.
	 *
	 * @see WP_Widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {

		$defaults = array(
			'id'  => $this->slider_id_default(),
		);

		// var_dump( $id_default[0] );

		$instance = wp_parse_args( $instance, $defaults );

		$this->widget_start( $args, $instance );

		if ( $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		echo do_shortcode( "[metaslider id={$instance['id']}]" );

		$this->widget_end( $args );
	}

	/**
	 * Slider
	 */
	public function slider_ids() {
		$ids = array();
		foreach ( $this->sliders() as $slider ) {
			$ids[ $slider->ID ] = $slider->post_title;
		}

		return $ids;
	}

	/**
	 * Slider
	 */
	public function slider_id_default() {
		$id_default = array();
		foreach ( $this->sliders() as $slider ) {
			$id_default[] = $slider->ID;
		}

		if ( empty( $id_default[0] ) ) {
			return 1;
		}

		return $id_default[0];
	}
}
