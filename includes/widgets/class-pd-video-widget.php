<?php
/**
 * Widget class.
 *
 * @package Phoenix_Digi
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * PD Video Widget.
 *
 * Show video.
 *
 * @author   NamNCN
 * @category Widgets
 * @package  PDCORE/Widgets
 * @version  1.0.0
 * @extends  PD_Widget
 */
class PD_Video_Widget extends PD_Widget {

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->widget_cssclass    = 'pd_video_widget';
		$this->widget_description = esc_html__( "Hiển thị video.", 'pd-theme' );
		$this->widget_id          = 'pd_video_widget';
		$this->widget_name        = esc_html__( 'PD: Video', 'pd-theme' );
		$this->settings           = array(
			'title'  => array(
				'type'  => 'text',
				'std'   => esc_html__( 'Video', 'pd-theme' ),
				'label' => esc_html__( 'Tiêu đề:', 'pd-theme' ),
			),
			'image' => array(
				'type'   => 'image',
				'std'    => '',
				'label'  => esc_html__( 'Hình ảnh video:', 'pd-theme' ),
			),
			'icon' => array(
				'type'  => 'text',
				'std'   => 'fa fa-play-circle-o',
				'label' => esc_html__( 'FontAwesome Icon:', 'pd-theme' ),
			),
			'link' => array(
				'type'   => 'text',
				'std'    => 'https://www.youtube.com/watch?v=moUqHH9O5Fs',
				'label'  => esc_html__( 'Link dẫn đến video:', 'pd-theme' ),
			),
		);

		parent::__construct();
	}

	/**
	 * Output widget.
	 *
	 * @see WP_Widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {

		$defaults = array(
			'image' => '',
			'icon'  => 'play-circle-o',
			'link'  => 'https://www.youtube.com/watch?v=moUqHH9O5Fs',
		);

		$instance = wp_parse_args( $instance, $defaults );

		$this->widget_start( $args, $instance );

		if ( $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		if ( $instance['image'] ) : ?>

		<div class="pd__video">
			<a href="<?php echo esc_url( $instance['link'] ); ?>" target="_blank">
				<?php if ( $instance['icon'] ) : ?>
				<div class="pd__video--overlay">
					<i class="<?php echo esc_attr( $instance['icon'] ); ?>"></i>
				</div>
				<?php endif; ?>

				<?php echo wp_get_attachment_image( $instance['image'], 'full' ); ?>
			</a>
		</div>

		<?php
		endif;

		$this->widget_end( $args );
	}
}
