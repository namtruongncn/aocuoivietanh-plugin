<?php
/**
 * Plugin Name: PhoenixDigi Core
 * Plugin URI: https://bitbucket.org/namtruongncn/phoenixdigi-core
 * Author: Nam NCN
 * Author URI: http://phoenixdigi.vn/gioi-thieu/
 * Version: 1.0.0
 * Description: This plugin is mandatory install for PhoenixDigi Theme. It will bring power to your website, if you do not install this plugin, your website is just like a simple blog.
 * Requires at least: 4.5
 *
 * Text Domain: phoenixdigi
 * Domain Path: /languages/
 *
 * @package Phoenix_Digi
 * @author NamNCN
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit(); // Exit if accessed directly.
}

if ( defined( 'PD_CORE_VERSION' ) ) {
	return;
}

define( 'PD_CORE_VERSION', '1.0.0' );
define( 'PD_CORE_FILE', __FILE__ );
define( 'PD_CORE_PATH', plugin_dir_path( PD_CORE_FILE ) );
define( 'PD_CORE_URL', plugin_dir_url( PD_CORE_FILE ) );

require_once PD_CORE_PATH . 'class-pd-core.php';

if ( ! function_exists( 'pdcore' ) ) {
	/**
	 * Get plugin instance.
	 *
	 * @return PD_CORE
	 */
	function pdcore() {
		return PD_CORE::get_instance();
	}
}
$GLOBALS['pdcore'] = pdcore();
