<?php
/**
 * PDFramework
 * This file is a part of PDFW.
 *
 * @package PDFW
 */

/**
 * Exit if accessed directly
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * PDFW_Customizer
 */
class PDFW_Customizer {

	public function add_control( $id, $args ) {

	}

	/**
	 * Add the panel using the Customizer API
	 *
	 * @param string $id   The panel ID.
	 * @param array  $args The panel arguments.
	 */
	public function add_panel( $id, $args ) {
		global $wp_customize;

		$wp_customize->add_panel( $id, $args );
	}

	/**
	 * Add the panel using the Customizer API
	 *
	 * @param string $id   The panel ID.
	 * @param array  $args The panel arguments.
	 */
	public function add_section( $id, $args ) {
		global $wp_customize;

		$wp_customize->add_section( $id, $args );
	}
}
