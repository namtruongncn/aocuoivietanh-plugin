<?php
/**
 * Plugin Name: PDFramework
 * Description: NCNTeam framework, thanks for Codestars Framework
 * Plugin URI: #
 * Author: Nam NCN
 * Author URI: #
 * Version: 1.0.0
 * License: GPL2
 * Text Domain: phoenixdigi
 */

require_once dirname( __FILE__ ) . '/inc/helpers.php';

if ( ! defined( 'CS_VERSION' ) ) {
	require_once dirname( __FILE__ ) . '/cs-framework/cs-framework-path.php';
}

if ( ! class_exists( 'PDFW' ) ) {
	require_once dirname( __FILE__ ) . '/inc/pdfw.php';
}

if ( ! function_exists( 'pdfw' ) ) :
	/**
	 * PDFramework Start
	 */
	$pdfw = new PDFW;

	/**
	 * //
	 *
	 * @param  string $make //.
	 * @return mixins
	 */
	function pdfw( $make = null ) {
		$pdfw = PDFW::get_instance();
		return is_null( $make ) ? $pdfw : $pdfw[ $make ];
	}
endif;
