<?php

global $pd_importers;

if ( ! function_exists( 'pd_importer_init' ) ) :
	/**
	 * Initial PD-Importer
	 *
	 * @todo Improve this function!!!
	 */
	function pd_importer_init() {
		$class = array( new PD_Importer_Manager, 'dispatch' );
		$description = esc_html__( 'Import demo content from pdcore by one click.', 'phoenixdigi' );

		register_importer( 'pd-importer', esc_html__( 'PD: Importer', 'phoenixdigi' ), $description, $class );
	}
endif;
add_action( 'admin_init', 'pd_importer_init', 999 );

if ( ! function_exists( 'pd_importer_register' ) ) :
	/**
	 * //
	 *
	 * @param  string $id   //.
	 * @param  array  $args //.
	 */
	function pd_importer_register( $id, array $args ) {
		global $pd_importers;

		$id = sanitize_key( $id );

		$args = wp_parse_args( $args, array(
			'name'        => '',
			'preview'     => '',
			'screenshot'  => '',
			'archive'     => '',
		) );

		$pd_importers[ $id ] = $args;
	}
endif;

if ( ! function_exists( 'pd_importers' ) ) :
	/**
	 * Get registered pd-importer
	 *
	 * @return array
	 */
	function pd_importers() {
		global $pd_importers;

		return is_null( $pd_importers ) ? array() : $pd_importers;
	}
endif;
add_filter( 'pd_importer_metadata', 'pd_importers', 20 );
